// class Flowers {
//     constructor(name, color, price) {
//         this.name = name;
//         this.color = color;
//         this.price = price;
//     }
// }
// function Flowers(name,color,price){
//     this.name=name;
//     this.color=color;
//     this.price=price;
// }
// let flower = new Flowers("liliya", "pink", 150)
// console.log(flower);

// import React from "react";
// import ReactDOM from "react-dom/client";
// class Counter extends React.Component {
//   render() {
//     return <div>Counter</div>
//   }
// }
// export default class App extends React.Component {
//   render() {
//     return (
//       <div><Counter /></div>
//     )
//   }
// }
// ReactDOM.createRoot(document.getElementById("root")).render(<App />)


// class Animal{
  // constructor(name){
  //   this.name=name;
  //   this.speed=0;
  // }
  // run(speed){
  //   this.speed=speed;
  // }
//   constructor(){
//     alert(`${this.name}`)
//   }
// }
// let animal=new Animal("Hello");
// console.log(animal);

// class Article {
//   constructor(title, date) {
//     this.title = title;
//     this.date = date;
//   }

//   static compare(articleA, articleB) {
//     return articleA.date - articleB.date;
//   }
// }

// // usage
// let articles = [
//   new Article("HTML", new Date(2019, 1, 1)),
//   new Article("CSS", new Date(2019, 0, 1)),
//   new Article("JavaScript", new Date(2019, 11, 1))
// ];

// articles.sort(Article.compare);

// alert(articles[0].title);
// import React from "react";
// import ReactDOM  from "react-dom/client";
// class Semantic{
//   header(){
//    return (
//     <div>Salom</div>
//    )
  
//   }
// }
// ReactDOM.createRoot(document.getElementById("root")).render(<header/>)

import React from "react";
import ReactDOM from "react-dom/client";

class Semantic extends React.Component {
  render() {
    return(
      <div className="wrapper">
        <div>
          <header className="header">&lt; header &gt;</header>
          <nav className="nav">&lt; nav &gt;</nav>
        </div>
        <div class="internal__wrapper">
          <div>
            <section className="section">&lt; section &gt;</section>
            <article className="article"> &lt; article &gt;</article>
          </div>
          <aside className="aside">&lt; aside &gt;</aside>
        </div>
        <footer className="footer">&lt; footer &gt;</footer>
      </div>
      ) 
  }
}

export default class App extends React.Component {
  render() {
    return (
      <div>
        <Semantic />
      </div>
    );
  }
}

ReactDOM.createRoot(document.getElementById("root")).render(<App />);